# README #

### Overview ###

A JSON-Library for Delphi. It started as a simple low level library to read and write JSON. It is currently able to generate delphi source code for de-/serialization from JSON.

### Prerequisites ###

* Delphi 2010+
* BeSharp.NET from Jeroen Plumiers for generating delphi units

### Contribution guidelines ###

* Any help is appreciated
